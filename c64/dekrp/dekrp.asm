curr		= $fb
prev		= $fc
temp		= $fd
srcindex	= $fe
color		= $52

screen		= $400

			* = screen

			.enc	"screen"
			.text	"CB74DAED7ECFE9E1 F0FA0DA2E7F920797 AFD9ED3EBF475DAED0EBB41 797ABA5D9E CFF73F1 0325C06B5CB6936BB0CA2F3D3E8 B43B045A86E9F337889D9EEFF73A1 09A4E8 7CF75AFD7FD7E77C1 DFEC747A9E7699D3 AF7579BEAECFE5E9F57B0E7FB3D3E979FA 45CFBFE1 E1 F43A4C07"

			.align	$400,$20

			* = $801

			.byte	$b,$8,$40,0,$9e,$32,$30,$36,$32,0

			* = 2062
startup:
			sei
			lda	#1
			sta	color
			sta	$d020
			lda	#0
			sta	srcindex
			sta	$d021
			
			lda	#<(screen+(40*19))
			sta	dstindex
			sta	dstcolor
			lda	#>(screen+(40*19))
			sta	dstindex+1
			lda	#>($d800+(40*19))
			sta	dstcolor+1
			;sty	dstindex

			lda	#<(screen+(40*19))
			sta	clrindex
			lda	#>(screen+(40*19))
			sta	clrindex+1
			lda	#$20
			ldx	#40*5
-
clrindex = * + 1
			sta	screen+(40*19)
			inc	clrindex
			bne	+
			inc	clrindex+1			
+			dex
			bne	-

			;lda	#$5b
			;sta	$d011
			lda	#$17
			sta	$d018

			dec	$d6
			dec	$d6
			dec	$d6

loop1:		jsr	loop
			bcc	loop1

			lda	#0
			sta	$d020
			cli
done_up:
			rts

loop:

			jsr	getNextOctet	; octet 1
			beq	done_up

			lda	curr
			sta	prev
			jsr	writeGsmChar	; septet 1

			jsr	getNextOctet	; octet 2
			beq	done_up

			lda	prev
			rol
			lda	curr
			sta	prev
			rol

			jsr	writeGsmChar	; septet 2

			jsr	getNextOctet	; octet 3
			beq	done_up

			lda	prev
			rol
			sta	temp
			lda	curr
			sta	prev
			rol
			sta	curr

			lda	temp
			rol
			lda	curr
			rol

			jsr	writeGsmChar	; septet 3

			jsr	getNextOctet	; octet 4
			beq	done_down

			lda	prev
			rol
			sta	temp
			lda	curr
			sta	prev
			rol
			sta	curr

			rol	temp
			rol curr

			lda	temp
			rol
			lda	curr
			rol

			jsr	writeGsmChar	; septet 4

			jsr	getNextOctet	; octet 5
			beq	done_up

			lda	curr
			tax
			ror
			sta	curr
			ror prev
			ror	curr
			ror	prev
			lda	curr
			ror
			lda	prev
			stx	prev
			ror
			lsr
			jsr	writeGsmChar	; septet 5

			jsr	getNextOctet	; octet 6
			beq	done_down

			lda	curr
			tax
			ror
			sta	curr
			ror	prev
			lda	curr
			ror
			lda	prev
			stx	prev
			ror
			lsr
			jsr	writeGsmChar	; septet 6

			jsr	getNextOctet	; octet 7
			beq	done_down

			lda	curr
			tax
			ror
			sta	curr
			lda	prev
			stx	prev
			ror
			lsr
			jsr	writeGsmChar	; septet 7

			lda	prev
			lsr
			jsr	writeGsmChar	; septet 8

			jmp	loop

done_down:	rts


getNextOctet:
			ldy	srcindex
-			lda	color
			sta	$d800,y
			lda	screen,y
			iny
			cmp	#$20
			bne	not_space
			inc	color
;			lda	#$80
;			sta	marker
			cpy	#0
			bne	-
			sec
			rts

not_space:
			and	#$7f
			tax
			lda	hextableH,x
			sta	curr
-			lda	color
			sta	$d800,y
			lda	screen,y
			iny
			cmp	#$20
			bne	not_space2
			inc	color
;			lda	#$80
;			sta	marker
			cpy	#0
			bne	-
			sec
			rts
not_space2:
			sty	srcindex
			and	#$7f
			tax
			lda	hextableL,x
			ora	curr
			sta	curr
			rts


writeGsmChar:
			and	#$7f
			tax
			lda	gsmtable,x
;marker = * + 1
;			ora	#0
dstindex = * + 1
			sta	screen+(40*19)
			lda	color
dstcolor = * + 1
			sta	$d800+(40*19)
;			lda	#0
;			sta	marker
			inc	dstcolor
			inc	dstindex
			bne	+
			inc	dstcolor+1
			inc	dstindex+1
+
			rts

	
			.align	$100

hextableL:
			.byte	0,$a,$b,$c,$d,$e,$f,0,0,0,0,0,0,0,0,0
			.byte	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
			.byte	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
			.byte	0,1,2,3,4,5,6,7,8,9,0,0,0,0,0,0
			.byte	0,$a,$b,$c,$d,$e,$f,0,0,0,0,0,0,0,0,0
			.byte	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
			.byte	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
			.byte	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0

hextableH:
			.byte	0,$a0,$b0,$c0,$d0,$e0,$f0,0,0,0,0,0,0,0,0,0
			.byte	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
			.byte	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
			.byte	0,$10,$20,$30,$40,$50,$60,$70,$80,$90,0,0,0,0,0,0
			.byte	0,$a0,$b0,$c0,$d0,$e0,$f0,0,0,0,0,0,0,0,0,0
			.byte	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
			.byte	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
			.byte	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0

gsmtable:
			.text	"  $             "
			.text	"                "
			.text	" ! # %&'()*+,-./"
			.text	"0123456789:;<=>?"
			.text	"!abcdefghijklmno"
			.text	"pqrstuvwxyz[]   "
			.text	"?ABCDEFGHIJKLMNO"
			.text	"PQRSTUVWXYZ{}   "
